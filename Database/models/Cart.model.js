import { Schema, SchemaTypes, model } from "mongoose";

const SchemaCart=new Schema({
    user:{type:SchemaTypes.ObjectId ,ref:"user"},
    CartItem:[
        {
            product:{ type:SchemaTypes.ObjectId ,ref:"product"},
            quantity:{type:Number,default:1},
            price:Number

        }
    ],
    totalPrice:{type:Number,default:0},
    totalPriceAfterDiscount:Number,
    discount:Number
})
export const CartModel=model('Cart',SchemaCart)