import { Schema,model } from "mongoose";
const Schemareview=new Schema({
    text:{
        type:String,
        required:true,
        trim:true,
    }, product:{
        type:Schema.ObjectId,
        ref:'product',
        required:true
    }, user:{
        type:Schema.ObjectId,
        ref:'user',
        required:true
    }
    , rating:{
        type:Number,
        min:1,
        max:5,
        default:0
    }
},{timestamps:true})
Schemareview.pre(/^find/,function(){
    this.populate('user','name')
})

export const reviewModel=model('review',Schemareview)

