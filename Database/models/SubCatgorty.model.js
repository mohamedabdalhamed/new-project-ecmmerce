import { Schema,model } from "mongoose";
const SchemaSubCategory=new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        minlength:[2,'short Too'],
    }, slug:{
        type:String,
        required:true,
        lowercase:true
    }, category:{
        type:Schema.ObjectId,
        required:true,
        ref:"category"

    }
},{timestamps:true})
export const SubCategoryModel=model('SubCategory',SchemaSubCategory)

