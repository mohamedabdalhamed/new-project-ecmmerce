import { Schema,model } from "mongoose";
const Schemacoupon=new Schema({
    code:{
        type:String,
        unique:true,
        required:true,
        trim:true 
    }, expire:{
        type:Date,
        required:true
    }, discount:{
        type:Number,
        min:0,
        default:0,
        required:true
    }
   
},{timestamps:true})
export const couponModel=model('coupon',Schemacoupon)

