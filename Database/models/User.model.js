import { Schema,model } from "mongoose";
import bcrypt from 'bcrypt'
import { SchemaTypes } from "mongoose";

const Schemauser=new Schema({
    name:{
        type:String,
        required:true,
        trim:true,
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,

    },
    password:{
        type:String,
        required:true,
        minlenght:[6,'not enoghe']
    },
    passwordChangeAt:{
        type:Date,
        required:true,
        default:Date.now()
    },
    verify:{
        type:Boolean,
        default:false
    },
    isactive:{
        type:Boolean,
        default:true,
    },
    blocked:{
        type:Boolean,
        default:false,
    },
    role:{
        type:String,
       enum:['admin','user'],
       default:'user'
    }
    
     , logo:{
        type:String,
       },
       wishlist:[{  type:SchemaTypes.ObjectId,
        ref:'product',
    
    }],
    addresses:[{
        city:String,
        street:String,
        phone:String
        }
    ]
},{timestamps:true})

Schemauser.pre('save',function() {
    this.password=bcrypt.hashSync(this.password,8)
    })
    Schemauser.pre('findOneAndUpdate',function() {
        if(this._update.password)
{
    this._update.password=bcrypt.hashSync(this._update.password,8)
}       
        })
       

     
export const userModel=model('user',Schemauser)

