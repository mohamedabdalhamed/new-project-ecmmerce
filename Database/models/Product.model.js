import { Schema,model } from "mongoose";
const Schemaproduct=new Schema({
    title:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        minlength:[2,'short Too'],

    }, slug:{
        type:String,
        required:true,
        lowercase:true
    }, imageCover:{
        type:String,
       // required:true
    },
    images:{
        type:[String],
       // required:true
    },
    Descrption:{
        type:String,
        required:true,
        minlength:[10,'u need more'],
        maxlength:[500,'max'],
    },
    price:{
        type:Number,
        default:0,
        min:0
    },
    
    priceAfterDiscount:{
        type:Number,
        default:0,
        min:0
    },
    
    Discount:{
        type:Number,
        default:0,
        min:0
    },
    category:{
        type:Schema.ObjectId,
        required:true,
        ref:"category"

    },
    brand:{
        type:Schema.ObjectId,
        required:true,
        ref:"brand"

    },
    SubCategory:{
        type:Schema.ObjectId,
        required:true,
        ref:"SubCategory"

    },
    CreatedAt:{
        type:Schema.ObjectId,
        required:true,
        ref:"user"

    },
    
    ratingAvg:{
        type:Number,
        min:1,
        max:5
    },
    ratingCount:{
        type:Number,
        min:0
    },
    quantity:{
        type:Number,
        default:0,
        min:0
    },

    solditem:{
        type:Number,
        default:0,
        min:0
    },
    


},{timestamps:true,toJSON:{ virtuals: true }})
Schemaproduct.post('init',(doc)=>{
 if(doc.title){
    const {title}=doc
    doc.slug=title.replace(/ /g,"_");
 }
if(doc.imageCover) {doc.imageCover=process.env.URLBase+'product/'+doc.imageCover  }
if(doc.imageCover){
doc.images.forEach((element,index) => {
  doc.images[index]=process.env.URLBase+'product/'+doc.images[index]    
  
}); 
    }})
    Schemaproduct.virtual('Myreview', {
        ref: 'review',
        localField: '_id',
        foreignField: 'product',
      });
      Schemaproduct.pre(/^find/,function(){
        this.populate('Myreview')
    })
  
export const productModel=model('product',Schemaproduct)

