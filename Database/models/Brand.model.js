import { Schema,model } from "mongoose";
const Schemabrand=new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
        trim:true,
    }, slug:{
        type:String,
        required:true,
        lowercase:true
    }, logo:{
        type:String,
    }
},{timestamps:true})
export const brandModel=model('brand',Schemabrand)

