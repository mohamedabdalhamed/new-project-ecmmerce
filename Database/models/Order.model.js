import { Schema, SchemaTypes, model } from "mongoose";

const SchemaOrder=new Schema({
    user:{type:SchemaTypes.ObjectId ,ref:"user"},
    CartItem:[
        {
            product:{ type:SchemaTypes.ObjectId ,ref:"product"},
            quantity:Number,
            price:Number

        }
    ],
    totalOrderPrice:{type:Number,default:0},
    ShippingAddress:{
           city:String,
        street:String,
         phone:String
        },
        PaymentMethod:{
            type:String,
            enum:['cash','card'],
            default:'cash'
        },
        ispaid:{
            type:Boolean,
            default:false
        },
        isDelivery:{
            type:Boolean,
            default:false
        },
        paidAt:Date,
        isDelivery:Date
})
export const OrderModel=model('Order',SchemaOrder)