import { Schema,model } from "mongoose";
const SchemaCategory=new Schema({
    name:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        minlength:[2,'short Too'],
    }, slug:{
        type:String,
        required:true,
        lowercase:true
    }, image:{
        type:String,
    }     
},{timestamps:true})
SchemaCategory.pre('init',(doc)=>{
    const {name}=doc
  doc.slug=name.replace(/ /g,"_");
  
      })
      SchemaCategory.post('init',(doc)=>{
      doc.image=process.env.URLBase +doc.image
      
          })
export const CategoryModel=model('category',SchemaCategory)

