export class ApiFeature{
    constructor(mongooseQuery,QueryString){
        this.mongooseQuery=mongooseQuery
        this.QueryString=QueryString
    }
    pagination(){
        const LIMIT=5
        let page= this.QueryString.page *1 || 1
        if(page <=0 )page=1
        let SKIP=(page-1)*LIMIT
        this.page=page
        this.mongooseQuery.limit(LIMIT)
        .skip(SKIP)
        
        return this
    }   
    filter(){
        
    let filterObject={...this.QueryString}
    const execuld=['page','sort','field','limit','keyword']

    execuld.forEach((value)=> delete filterObject[value] )

    filterObject=JSON.stringify(filterObject)
     filterObject=filterObject.replace(/\b(gt| gte | lt |lte)\b/g,match=>`$${match}`)
     filterObject=JSON.parse(filterObject)
      this.mongooseQuery.find(filterObject)
      return this
    }
    sort(){
        if(this.QueryString.sort){
        let SortyBy=this.QueryString.sort.split(',').join(' ')
        this.mongooseQuery.sort(SortyBy)
    }
    return this
}
    search(){
        if(this.QueryString.keyword){

         this.mongooseQuery.find(    {
                 $or: [{title:{$regex:this.QueryString.keyword,$options:'i'}}
                 ,{Descrption:{$regex:this.QueryString.keyword,$options:'i'}}]
                
                },
        )
    }
        return this

}
field(){
    
if(this.QueryString.field)    
{
    let fields=this.QueryString.field.split(',').join(' ')
    this.mongooseQuery.select(fields)
}
return this
}


}