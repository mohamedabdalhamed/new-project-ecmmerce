import { AppError } from "./AppError.js"
import { CatchError } from "./CatchError.js"

export const DeleteOne=(model)=>{
    return CatchError(async (req,res,next)=>{
        const {id}=req.params
        const DeleteSubCategory= await model.findOneAndDelete({_id:id})
        !DeleteSubCategory && next(new AppError('Not Found Item',404))
        DeleteSubCategory && res.json({message:"Success",DeleteSubCategory})
        
    })
}
export const pagination=(Model)=>{
  return  CatchError(async(req,res)=>{
        const LIMIT=5
        let page= req.query.page *1 || 1
        if(page <=0 )page=1
        let SKIP=(page-1)*LIMIT
    console.log(req.query)
        const data=await Model.find().limit(LIMIT)
        .skip(SKIP)
        const count=await Model.count();
        let nextpage=page+1
        let totalPages= Math.ceil(count / LIMIT)
        if(totalPages =>nextpage){nextpage=totalPages}
        
        res.json({message:"success",data,  totalPages: totalPages,
        currentPage: page,nextPage:nextpage})
        })

}