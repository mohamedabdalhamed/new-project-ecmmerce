import express from "express";
import * as Controls from "./Cart.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const cartRouter=express.Router();
cartRouter.route('/').post(Auth,AllowTo('user'),Controls.Addcart).get(Auth,AllowTo('user'),Controls.Getcart).delete(Auth,AllowTo('user'),Controls.DeleteCarts)
cartRouter.route('/:id').put(Auth,AllowTo('user'),Controls.Updatecart).delete(Auth,AllowTo('user'),Controls.RemoveItem)
cartRouter.route('/ApplyCoupon').post(Auth,AllowTo('user'),Controls.ApplyCoupon)
