import { CartModel } from "../../../Database/models/Cart.model.js";
import { couponModel } from "../../../Database/models/Coupon.model.js";
import { productModel } from "../../../Database/models/Product.model.js";
import { AppError } from "../../uilt/AppError.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";
const TotalPrices=(cart)=>{
    let totalPrice=0
    cart.CartItem.map(element=> totalPrice+=element.price*element.quantity)
    cart.totalPrice=totalPrice
}
const Addcart=CatchError(async(req,res,next)=>{
const existCart=await CartModel.findOne({user:req.userId})
let product=await productModel.findById(req.body.product).select('price')
console.log(product)
if(!product) {return next(new AppError('Not Found This Product',404))} 
req.body.price=product.price
if(!existCart){
const cart=new CartModel({user:req.userId,CartItem:[req.body]})
TotalPrices(cart)

await cart.save()
  !cart &&   next(new AppError('faild',404)) 
  cart &&    res.status(201).json({message:"success",cart})
}
let item=existCart.CartItem.find(element => element.product == req.body.product)
    if(item){
        item.quantity+= req.body.quantity || 1
        item.price=product.price
    }
    else{
        existCart.CartItem.push(req.body)
        
    }
    TotalPrices(existCart)
    await existCart.save()

    res.status(201).json({message:"success",existCart})

})
const Getcart=CatchError(async(req,res)=>{
    let  result=await CartModel.find({user:req.userId}).populate('CartItem.product')
    console.log(req.userId)
res.json({message:"success",result})
})
const RemoveItem=CatchError(async(req,res,next)=>{
    const existCart=await CartModel.findOneAndUpdate({user:req.userId},{$pull:{CartItem:{_id:req.params.id}}},{new:true})
    !existCart &&   next(new AppError('faild',404))
    TotalPrices(existCart)
    existCart &&     res.status(201).json({message:"success",existCart})
})
const DeleteCarts=DeleteOne(CartModel)
const ApplyCoupon=CatchError(async(req,res,next)=>{
   const coupon= await couponModel.findOne({code:req.body.code,expire:{$gt:Date.now()}})
   console.log(coupon)
   !coupon &&   next(new AppError('faild',404)) 
    const existCart=await CartModel.findOne({user:req.userId})
    let totalPriceAfterDiscount= existCart.totalPrice - (existCart.totalPrice * coupon.discount)/100
    existCart.totalPriceAfterDiscount=totalPriceAfterDiscount
    existCart.discount=coupon.discount
    await existCart.save()
    res.json({message:"success",existCart})

})
const Updatecart=async(req,res,next)=>{
        const {id}=req.params

        const cart=await CartModel.findOneAndUpdate({_id:id},req.body,{new:true})
        !cart &&   next(new AppError('faild',404)) 
        cart &&    res.status(201).json({message:"success",cart})
    }
export {Addcart,Getcart,Updatecart,RemoveItem,DeleteCarts,ApplyCoupon}
