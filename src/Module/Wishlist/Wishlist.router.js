import express from "express";
import * as Controls from "./Wishlist.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const wishlistRouter=express.Router();
wishlistRouter.route('/').patch(Auth,AllowTo('user'),Controls.AddWishe).get(Auth,AllowTo('user'),Controls.GetWishList).delete(Auth,AllowTo('user'),Controls.RemoveWishList)
