import { userModel } from "../../../Database/models/User.model.js";
import { AppError } from "../../uilt/AppError.js";
import { CatchError } from "../../uilt/CatchError.js";

const AddWishe=CatchError(async(req,res,next)=>{
    const {product}=req.body
    
const Wishlist=await userModel.findByIdAndUpdate(req.userId,{$addToSet:{wishlist:product}},{new:true})
return res.status(201).json({message:"success",Wishlist})
})

const GetWishList=CatchError(async(req,res,next)=>{
    
const Wishlist=await userModel.findOne({_id:req.userId}).populate('wishlist')
return res.status(201).json({message:"success",Wishlist})
})

const RemoveWishList=CatchError(async(req,res,next)=>{
    const {product}=req.body
    
const Wishlist=await userModel.findByIdAndUpdate(req.userId,{$pull:{wishlist:product}},{new:true})
return res.status(201).json({message:"success",Wishlist})
})
export {AddWishe,GetWishList,RemoveWishList}
