import { SubCategoryModel } from "../../../Database/models/SubCatgorty.model.js";
import { AppError } from "../../uilt/AppError.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";

 const AddSubCategory=CatchError(async(req,res,next)=>{
    const {name,category}=req.body
    const slug=name.replace(/ /g,"_");
    const SubCategorys=await SubCategoryModel.findOne({slug:slug})
    // if(Categorys) return res.json({error:"Category "})
    const SubCategory=new SubCategoryModel({name,slug:slug,category})
    const dataSubCategory= await SubCategory.save()
    
    return res.status(201).json({message:"success",dataSubCategory})
    })
    const GetSubCategory=CatchError(async(req,res)=>{
        let filter={}
        if(req.params.category){filter=req.params}
        console.log(filter)
        const datas={page:1,limit:1}
        const { page = 1, limit = 1 } =req.query
console.log(req.query)
        const data=await SubCategoryModel.find(filter).limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
        const count=await SubCategoryModel.count();
        res.json({message:"success",data,  totalPages: Math.ceil(count / limit),
        currentPage: page})
        })
        
    const GetSubCategoryDetails=CatchError(async(req,res)=>{
        const {id}=req.params
        const data=await SubCategoryModel.find({category:id})
        res.json({message:"success",data})
        })
    const  DeleteSubCagteroy=DeleteOne(SubCategoryModel)
    const UpdateSubCategory=CatchError(async(req,res,next)=>{
        const {name,category}=req.body
        const slug=name.replace(/ /g,"_");
        const {id}=req.params
        const UpdateSubCtegory= await SubCategoryModel.findByIdAndUpdate(id,{name,category,slug:slug},{new:true})
        !UpdateSubCtegory && next(new AppError('Not Found Item',404))
        UpdateSubCtegory && res.json({message:"Success",UpdateSubCtegory})  
    })
export {AddSubCategory,GetSubCategory,GetSubCategoryDetails,DeleteSubCagteroy,UpdateSubCategory}