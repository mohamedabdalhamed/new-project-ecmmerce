import express from 'express'
import * as Controls from './SubCategory.control.js';
import { AddSubCategory } from './SubCategory.validate.js';
import { validateMaddelware } from '../../maddelware/Validate.js';
export const useRouterSubCatgeory=express.Router({mergeParams:true});
useRouterSubCatgeory.route('/').get(Controls.GetSubCategory).post(validateMaddelware(AddSubCategory),Controls.AddSubCategory)
useRouterSubCatgeory.route('/:id').get(Controls.GetSubCategoryDetails).delete(Controls.DeleteSubCagteroy).put(Controls.UpdateSubCategory)