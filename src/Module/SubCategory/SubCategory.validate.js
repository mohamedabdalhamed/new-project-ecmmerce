import Joi from "joi"
export const AddSubCategory= Joi.object({
name: Joi.string()
.min(3)
.required(),
category: Joi.string()
    .required(),
    }
    )
