import Joi from "joi"
export const AddCategory= Joi.object({
name: Joi.string()
.min(3)
.required(),
})
