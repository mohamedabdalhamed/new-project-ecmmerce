import { OrderModel } from "../../../Database/models/Order.model.js";
import { couponModel } from "../../../Database/models/Coupon.model.js";
import { productModel } from "../../../Database/models/Product.model.js";
import { AppError } from "../../uilt/AppError.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";
import { CartModel } from "../../../Database/models/Cart.model.js";
const TotalPrices=(Order)=>{
    let totalPrice=0
    Order.OrderItem.map(element=> totalPrice+=element.price*element.quantity)
    Order.totalPrice=totalPrice
}
const AddOrder=CatchError(async(req,res,next)=>{
const existCart=await CartModel.findOne({user:req.userId})
if(existCart){
    let prices=existCart.totalPriceAfterDiscount ?existCart.totalPriceAfterDiscount  :existCart.totalPrice
    let data=new OrderModel({user:req.userId,CartItem:existCart.existCart,totalOrderPrice:prices,PaymentMethod:req.body.PaymentMethod,paidAt:Date.now(),isDelivery:Date.now()})
await data.save() 

if(data){
    await productModel.bulkWrite(option) 
    let option=existCart.existCart.map(item=>({
        updateOne:{
            fliter:{    _id:item.product},
            update:{$inc:{quantity:-item.quantity,solditem:item.quantity}}
        }
    }))
}
 await CartModel.findOneAndDelete({user:req.userId})
 return res.json({message:"success",data})
}
return res.json({message:"falid"})



})

const GetAll=CatchError(async(req,res)=>{ 
    let  result=await OrderModel.find()
console.log(req.userId)
res.json({message:"success",result})})
const GetOrder=CatchError(async(req,res)=>{
    let  result=await OrderModel.find({user:req.userId})
    console.log(req.userId)
res.json({message:"success",result})
})
const RemoveItem=CatchError(async(req,res,next)=>{
    const existOrder=await OrderModel.findOneAndUpdate({user:req.userId},{$pull:{OrderItem:{_id:req.params.id}}},{new:true})
    !existOrder &&   next(new AppError('faild',404))
    TotalPrices(existOrder)
    existOrder &&     res.status(201).json({message:"success",existOrder})
})
const DeleteOrders=DeleteOne(OrderModel)
const ApplyCoupon=CatchError(async(req,res,next)=>{
   const coupon= await couponModel.findOne({code:req.body.code,expire:{$gt:Date.now()}})
   console.log(coupon)
   !coupon &&   next(new AppError('faild',404)) 
    const existOrder=await OrderModel.findOne({user:req.userId})
    let totalPriceAfterDiscount= existOrder.totalPrice - (existOrder.totalPrice * coupon.discount)/100
    existOrder.totalPriceAfterDiscount=totalPriceAfterDiscount
    existOrder.discount=coupon.discount
    await existOrder.save()
    res.json({message:"success",existOrder})

})
const UpdateOrder=async(req,res,next)=>{
        const {id}=req.params

        const Order=await OrderModel.findOneAndUpdate({_id:id},req.body,{new:true})
        !Order &&   next(new AppError('faild',404)) 
        Order &&    res.status(201).json({message:"success",Order})
    }
export {AddOrder,GetOrder,UpdateOrder,RemoveItem,DeleteOrders,ApplyCoupon,GetAll}
