import express from "express";
import * as Controls from "./Order.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const OrderRouter=express.Router();
OrderRouter.route('/').post(Auth,AllowTo('user'),Controls.AddOrder).get(Auth,AllowTo('user'),Controls.GetOrder).delete(Auth,AllowTo('user'),Controls.DeleteOrders)
OrderRouter.route('/:id').put(Auth,AllowTo('user'),Controls.UpdateOrder).delete(Auth,AllowTo('user'),Controls.RemoveItem)
OrderRouter.route('/Admin/Order').get(Auth,AllowTo('user'),Controls.GetAll)
