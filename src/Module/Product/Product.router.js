import express from "express";
import * as Controls from "./Product.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AddProductValidate } from "./Product.validate.js";
import { useRouterSubCatgeory } from "../SubCategory/SubCategory.route.js";
import { MaxOfFile } from "../../maddelware/uploads.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const ProductRoute=express.Router();
const arrayOfIAMGES=[{ name: 'imageCover', maxCount: 1 }, { name: 'images', maxCount: 8 }]
ProductRoute.use('/:categoryId/subcategories',useRouterSubCatgeory)
ProductRoute.route('/').post(Auth,AllowTo('user'),MaxOfFile(arrayOfIAMGES,'myuploads/product/'),validateMaddelware(AddProductValidate),Controls.AddProduct).get(Controls.GetProduct)
ProductRoute.route('/:id').put(Auth,AllowTo('user'),Controls.UpdateProduct).delete(Auth,AllowTo('user'),Controls.DeleteProduct).get(Controls.GetProductDetails)

