import { productModel } from "../../../Database/models/Product.model.js";
import { AppError } from "../../uilt/AppError.js";
import { ApiFeature } from "../../uilt/AppFeature.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";

const AddProduct=CatchError(async(req,res,next)=>{
const {title}=req.body
const slug=title.replace(/ /g,"_");
req.body.slug=slug
req.body.imageCover=req.files.imageCover[0].filename
let arrayDataImage=req.files.images
req.body.images=arrayDataImage.map(element => element.filename)
// const Products=await productModel.findOne({...req.body,slug})
// if(Products) return res.json({error:"Category "})
const Product=new productModel({...req.body})
await Product.save()

return res.status(201).json({message:"success",Product})
})

const GetProduct= CatchError(async(req,res)=>{

    let  ApiFeatures=new ApiFeature(productModel.find(),req.query).pagination().filter().sort().search().field()

   
    let result=await ApiFeatures.mongooseQuery
    res.json({message:"success",result })
    })
const GetProductDetails=(async(req,res)=>{
    const {id}=req.params

    const data=await productModel.findById(id)
    res.json({message:"success",data})
})

const DeleteProduct=DeleteOne(productModel)
    const UpdateProduct=CatchError(async(req,res)=>{
        const {id}=req.params
        const Product=await productModel.findByIdAndUpdate(id,req.body,{new:true})
        !Product &&   next(new AppError('faild',404)) 
        Product &&    res.status(201).json({message:"success",Product})
    })
export {AddProduct,GetProduct,UpdateProduct,DeleteProduct,GetProductDetails}
