import Joi from "joi"
export const AddProductValidate= Joi.object({
title:Joi.string().min(3).required(),
Descrption:Joi.string().min(10).max(500).required(),
price:Joi.number().required(),
priceAfterDiscount:Joi.number(),
Discount:Joi.number(),
category:Joi.string().required(),
brand:Joi.string().required(),
SubCategory:Joi.string().required(),
CreatedAt:Joi.string().required(),
ratingAvg:Joi.number().min(1).max(5),
ratingCount:Joi.number(),
stock:Joi.number().required(),
solditem:Joi.number()
})

