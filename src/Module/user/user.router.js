import express from "express";
import * as Controls from "./user.control.js";

export const userRoute=express.Router();
userRoute.route('/').post(Controls.Adduser).get(Controls.Getuser)
userRoute.route('/:id').put(Controls.Updateuser).delete(Controls.Deleteuser).get(Controls.GetuserDetails)   

userRoute.patch("/ChangePassword/:id",Controls.ChangePassword)
