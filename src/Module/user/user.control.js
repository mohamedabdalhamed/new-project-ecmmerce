import { hashSync } from "bcrypt";
import { userModel } from "../../../Database/models/User.model.js";
import { AppError } from "../../uilt/AppError.js";
import { ApiFeature } from "../../uilt/AppFeature.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";
// const SignUp=CatchError(async(req,res,next)=>{
//         const {name,email,password}=req.body
//         const user=await userModel.find({email})
//         if(!user){
//             let hasPassword=bcrypt.hashSync(password,8)
//         }

// req.body.images=arrayDataImage.map(element => element.filename)
// // const users=await userModel.findOne({...req.body,slug})
// // if(users) return res.json({error:"Category "})
// const user=new userModel({...req.body,imageCover,images:FilesNames})
// await user.save()

// return res.status(201).json({message:"success",Product})
// })
const Adduser=CatchError(async(req,res,next)=>{

    const {name,email,password}=req.body
                const user=new userModel({name,email,password})
                await user.save()

        
    
    return res.status(201).json({message:"success",user})
    })

const Getuser= CatchError(async(req,res)=>{

    let  ApiFeatures=new ApiFeature(userModel.find(),req.query).pagination().filter().sort().search().field()

   
    let result=await ApiFeatures.mongooseQuery
    res.json({message:"success",result })
    })
const GetuserDetails=(async(req,res)=>{
    const {id}=req.params

    const data=await userModel.findById(id)
    res.json({message:"success",data})
})
const ChangePassword=CatchError(async(req,res)=>{
    const {id}=req.params
    const {password}=req.body
    let passwordChangeAt=Date.now()
    const user=await userModel.findByIdAndUpdate(id,{password,passwordChangeAt},{new:true})

    !user &&   next(new AppError('faild',404)) 
    user &&    res.status(201).json({message:"success",user})
})
const Deleteuser=DeleteOne(userModel)
    const Updateuser=CatchError(async(req,res)=>{
        const {id}=req.params
        const user=await userModel.findByIdAndUpdate(id,req.body,{new:true})
        !user &&   next(new AppError('faild',404)) 
        user &&    res.status(201).json({message:"success",user})
    })
export {Adduser,Getuser,Updateuser,Deleteuser,GetuserDetails,ChangePassword}
