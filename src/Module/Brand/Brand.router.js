import express from "express";
import * as Controls from "./Brand.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
export const BrandRouter=express.Router();
BrandRouter.route('/').post(Controls.AddBrand).get(Controls.GetBrand)
BrandRouter.route('/:id').put(Controls.UpdateCateory).delete(Controls.DeleteBrnad)

