import { brandModel } from "../../../Database/models/Brand.model.js";
import { AppError } from "../../uilt/AppError.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";

const AddBrand=CatchError(async(req,res,next)=>{
const {name}=req.body
const slug=name.replace(/ /g,"_");
// const Brands=await brandModel.findOne({slug:slug})
// if(Brands) return res.json({error:"Brand "})
const Brand=new brandModel({name,slug:slug})
await Brand.save()

return res.status(201).json({message:"success",Brand})
})
const GetBrand=CatchError(async(req,res)=>{
const data=await brandModel.find()
res.json({message:"success",data})
})

const DeleteBrnad=DeleteOne(brandModel)
    const UpdateCateory=async(req,res)=>{
        const {id}=req.params
        const {name}=req.body
        const slug=name.replace(/ /g,"_");

        const Brand=await brandModel.findByIdAndUpdate(id,{name,slug},{new:true})
        !Brand &&   next(new AppError('faild',404)) 
        Brand &&    res.status(201).json({message:"success",Brand})
    }
export {AddBrand,GetBrand,UpdateCateory,DeleteBrnad}
