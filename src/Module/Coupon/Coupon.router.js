import express from "express";
import * as Controls from "./Coupon.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const CouponRouter=express.Router();
CouponRouter.route('/').post(Auth,AllowTo('user'),Controls.AddCoupon).get(Controls.GetCoupon)
CouponRouter.route('/:id').put(Auth,AllowTo('user'),Controls.UpdateCoupon).delete(Controls.DeleteCoupon)

