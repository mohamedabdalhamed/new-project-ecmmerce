import { couponModel } from "../../../Database/models/Coupon.model.js";
import { AppError } from "../../uilt/AppError.js";
import { ApiFeature } from "../../uilt/AppFeature.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";
import qrcode from 'qrcode'
const AddCoupon=CatchError(async(req,res,next)=>{
    
const Coupon=new couponModel(req.body)
await Coupon.save()
let url =qrcode.toDataURL(Coupon.code)
  !Coupon &&   next(new AppError('faild',404)) 
  Coupon &&    res.status(201).json({message:"success",Coupon,url})
})
const GetCoupon=CatchError(async(req,res)=>{
    let  ApiFeatures=new ApiFeature(couponModel.find(),req.query).pagination().filter().sort().search().field()
    let result=await ApiFeatures.mongooseQuery

res.json({message:"success",result})
})

const DeleteCoupon=DeleteOne(couponModel)
    const UpdateCoupon=async(req,res,next)=>{
        const {id}=req.params

        const Coupon=await couponModel.findOneAndUpdate({_id:id},req.body,{new:true})
        !Coupon &&   next(new AppError('faild',404)) 
        Coupon &&    res.status(201).json({message:"success",Coupon})
    }
export {AddCoupon,GetCoupon,UpdateCoupon,DeleteCoupon}
