import express from "express";
import * as Controls from "./review.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const reviewRouter=express.Router();
reviewRouter.route('/').post(Auth,AllowTo('user'),Controls.Addreview).get(Controls.Getreview)
reviewRouter.route('/:id').put(Auth,AllowTo('user'),Controls.Updatereview).delete(Controls.Deletereview)

