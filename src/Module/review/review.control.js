import { reviewModel } from "../../../Database/models/review.model.js";
import { AppError } from "../../uilt/AppError.js";
import { ApiFeature } from "../../uilt/AppFeature.js";
import { CatchError } from "../../uilt/CatchError.js";
import { DeleteOne } from "../../uilt/factor.js";

const Addreview=CatchError(async(req,res,next)=>{
    
    req.body.user=req.userId
const reviews=await reviewModel.findOne({user:req.userId,product:req.body.product})
console.log(reviews)
if(reviews){return  next(new AppError('you Type Comment before',401)) }
const review=new reviewModel(req.body)
await review.save()

return res.status(201).json({message:"success",review})
})
const Getreview=CatchError(async(req,res)=>{
    let  ApiFeatures=new ApiFeature(reviewModel.find(),req.query).pagination().filter().sort().search().field()
    let result=await ApiFeatures.mongooseQuery

res.json({message:"success",result})
})

const Deletereview=DeleteOne(reviewModel)
    const Updatereview=async(req,res,next)=>{
        const {id}=req.params

        const review=await reviewModel.findOneAndUpdate({_id:id,user:req.userId,product:req.body.product},req.body,{new:true})
        !review &&   next(new AppError('faild',404)) 
        review &&    res.status(201).json({message:"success",review})
    }
export {Addreview,Getreview,Updatereview,Deletereview}
