import express from "express";
import * as Controls from "./Category.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AddCategoryvalidated, DeleteValidate, UpdateCateoryValidate } from "./Category.validate.js";
import { useRouterSubCatgeory } from "../SubCategory/SubCategory.route.js";
import { singleUploder } from "../../maddelware/uploads.js";
export const CategoryRouter=express.Router();
CategoryRouter.use('/:category/SubCategories',useRouterSubCatgeory)
CategoryRouter.route('/').post(singleUploder('imageCover','myuploads/'),validateMaddelware(AddCategoryvalidated),Controls.AddCategory).get(Controls.GetCategory)
CategoryRouter.route('/:id').put(validateMaddelware(UpdateCateoryValidate), Controls.UpdateCateory).delete(validateMaddelware(DeleteValidate),Controls.DeleteCagteroy)

