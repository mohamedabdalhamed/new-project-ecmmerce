import express from "express";
import * as Controls from "./address.control.js";
import { validateMaddelware } from "../../maddelware/Validate.js";
import { AllowTo, Auth } from "../../maddelware/Auth.js";
export const AddaddressRouter=express.Router();
AddaddressRouter.route('/').patch(Auth,AllowTo('user'),Controls.Addaddress).get(Auth,AllowTo('user'),Controls.Getaddress).delete(Auth,AllowTo('user'),Controls.Removeaddress)
