import { userModel } from "../../../Database/models/User.model.js";
import { AppError } from "../../uilt/AppError.js";
import { CatchError } from "../../uilt/CatchError.js";

const Addaddress=CatchError(async(req,res,next)=>{
const address=await userModel.findByIdAndUpdate(req.userId,{$addToSet:{addresses:req.body}},{new:true})
console.log(address)

return res.status(201).json({message:"success",addresses:address.addresses})
})

const Getaddress=CatchError(async(req,res,next)=>{
    
const address=await userModel.findOne({_id:req.userId})
return res.status(201).json({message:"success",address})
})

const Removeaddress=CatchError(async(req,res,next)=>{
    
const address=await userModel.findByIdAndUpdate(req.userId,{$pull:{addresses:{_id:req.body.address}}},{new:true})
return res.status(201).json({message:"success",address})
})
export {Addaddress,Getaddress,Removeaddress}
