import Jwt from "jsonwebtoken"
import { userModel } from "../../../Database/models/User.model.js"
import { CatchError } from "../../uilt/CatchError.js"
import { compareSync } from "bcrypt"
const SignUp=CatchError(async(req,res,next)=>{
        // const {name,email,password}=req.body
        //     const user=new userModel({name,email,password})
        //     const data=await user.save()
        //     console.log(...req.body)
        const user=new userModel(req.body)
        await user.save()
            const Token=Jwt.sign({name:user.name,userId:user._id,passwordChangeAt:user.passwordChangeAt},process.env.Jwt_Key)
        


return res.status(201).json({message:"success",user,Token})
})
const SignIn=CatchError(async(req,res,next)=>{
    const {email,password}=req.body
    const user=await userModel.findOne({email})
    if(user && compareSync(password,user.password)){
        let Token=Jwt.sign({name:user.name,userId:user._id,passwordChangeAt:user.passwordChangeAt},process.env.Jwt_Key)
        return res.status(200).json({message:"success",Token})

    }
    return res.status(500).json({message:"Password Or Email"})


})

export{SignUp,SignIn}
