import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';

export const singleUploder=(fieldName,FileNames)=>{
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, FileNames)
        },
        filename: function (req, file, cb) {
          cb(null,   uuidv4() + '-'  + file.originalname )
        }
      })
      function fileFilter (req, file, cb) {

        // The function should call `cb` with a boolean
        // to indicate if the file should be accepted
       // to indicate if the file should be accepted
  if(file.mimetype.startsWith('image')) 
  {   cb(null, true)}
  else{
      cb(null, false)
      cb(new Error('I don\'t have a clue!'))
  
  }
      // To reject this file pass `false`, like so:
    
      // To accept the file pass `true`, like so:
    
      // You can always pass an error if something goes wrong:
    
      
    
      }
      
      const upload = multer({ storage,fileFilter })
      return upload.single(fieldName)
}
export const MaxOfFile=(fieldName,FileNames)=>{
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, FileNames)
        },
        filename: function (req, file, cb) {
          cb(null,   uuidv4() + '-'  + file.originalname )
        }
      })
      function fileFilter (req, file, cb) {

        // The function should call `cb` with a boolean
        // to indicate if the file should be accepted
       // to indicate if the file should be accepted
  if(file.mimetype.startsWith('image')) 
  {   cb(null, true)}
  else{
      cb(null, false)
      cb(new Error('I don\'t have a clue!'))
  
  }
      // To reject this file pass `false`, like so:
    
      // To accept the file pass `true`, like so:
    
      // You can always pass an error if something goes wrong:
    
      
    
      }
      
      const upload = multer({ storage,fileFilter })
      return upload.fields(fieldName)
}