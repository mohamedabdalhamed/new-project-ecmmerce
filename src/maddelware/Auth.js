import jwt from 'jsonwebtoken'
import { AppError } from '../uilt/AppError.js'
import { userModel } from '../../Database/models/User.model.js'
export const Auth=(req,res,next)=>{
let Token=req.header('token')
jwt.verify(Token,process.env.Jwt_Key,async(error,decode)=>{
if(error){
    return res.json('error')
}
req.userId=decode.userId
// let timeEx=parseInt(time.getTime ()/1000)
// console.log(parseInt(Date(decode.passwordChangeAt).getTime()/1000),time)
// console.log(decode.passwordChangeAt)
const UserData=await userModel.findById(decode.userId)
// if(timeEx > UserData.passwordChangeAt){
//     return res.json(new AppError('U Change Password',401))
// }
req.user=UserData
next()

})
}

export const AllowTo=(...roles)=>{
return(req,res,next)=>{
    if(!roles.includes(req.user.role)){return next(new AppError('U not have access',401))}    
    next()
}
}