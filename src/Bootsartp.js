import { BrandRouter } from "./Module/Brand/Brand.router.js"
import { cartRouter } from "./Module/Cart/Cart.router.js"
import { CategoryRouter } from "./Module/Category/Category.router.js"
import { CouponRouter } from "./Module/Coupon/Coupon.router.js"
import { OrderRouter } from "./Module/Order/Order.router.js"
import { ProductRoute } from "./Module/Product/Product.router.js"
import { useRouterSubCatgeory } from "./Module/SubCategory/SubCategory.route.js"
import { wishlistRouter } from "./Module/Wishlist/Wishlist.router.js"
import { AddaddressRouter } from "./Module/address/address.router.js"
import { UseAuth } from "./Module/auth/auth.route.js"
import { reviewRouter } from "./Module/review/review.router.js"
import { userRoute } from "./Module/user/user.router.js"
import { MaddelwareError } from "./maddelware/MaddelwareError.js"
export function Bootsartp(app){
    app.get('/', (req, res) => res.send('Hello World!'))
app.use('/api/v1/Categories',CategoryRouter)
app.use('/api/v1/SubCategories',useRouterSubCatgeory)
app.use('/api/v1/Brands',BrandRouter)
app.use('/api/v1/Products',ProductRoute)
app.use('/api/v1/user',userRoute)
app.use('/api/v1/auth',UseAuth)
app.use('/api/v1/Review',reviewRouter)
app.use('/api/v1/wishlist',wishlistRouter)
app.use('/api/v1/Address',AddaddressRouter)
app.use('/api/v1/Coupon',CouponRouter)
app.use('/api/v1/cart',cartRouter)
app.use('/api/v1/Order',OrderRouter)


app.use(MaddelwareError)
}
