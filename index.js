import { configDotenv } from 'dotenv'
import express from 'express'
import {dbConnection} from './Database/dbConnection.js'
import { Bootsartp } from './src/Bootsartp.js'
configDotenv()

const app = express()
dbConnection()
const port = 3002
app.use(express.json())
app.use(express.static('myuploads'))
Bootsartp(app)
app.listen(port, () => console.log(`Example app listening on port ${port}!`))